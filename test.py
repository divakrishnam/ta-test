from multiprocessing import Pool
import requests
from datetime import datetime
import threading
import time
from multiprocessing import Process
import docker
import json

import logging
logging.basicConfig(filename='example.log', level=logging.DEBUG)

def call_api(id):
    start = datetime.now()
    response = requests.post(url="http://127.0.0.1:5003/predict", json={"message": "oppa"})
    finish = datetime.now()
    f = open("data.flask.txt", "a")
    # f = open("data.gunicorn.w4.txt", "a")
    f.write(f"{start}\t{finish}\t{response.elapsed.total_seconds()}\t{response.status_code}\n")
    f.close()

if __name__ == '__main__':
    for i in range(100):
        call_api(1)

# if __name__ == '__main__':
#     p = Pool(10)
#     for i in range(50):
#         p.map(call_api, [1,2,3,4,5,6,7,8,9,10])