import docker
import json
from datetime import datetime

# client = docker.from_env()
# print(client.containers.list())
# for i in client.containers.list():
#     container = i.stats(stream=False)
#     if container['name'] == '/frosty_cori':
#         for stat in i.stats():
#             container = json.loads(stat.decode('UTF-8'))
#             print("---------------------------")
#             print(container['read'])
#             print(container['cpu_stats']['cpu_usage']['total_usage'])
#             print(container['memory_stats']['usage'])
import dateutil.parser

def logging_container(name, filename):
    client = docker.from_env()
    print(client.containers.list())
    for i in client.containers.list():
        container = i.stats(stream=False)
        if container['name'] == name:
            for stat in i.stats():
                container = json.loads(stat.decode('UTF-8'))
                print("---------------------------")
                # print(container['read'])
                # print(container['preread'])
                # print(int(container['cpu_stats']['cpu_usage']['total_usage']))
                # print(container['precpu_stats']['cpu_usage']['total_usage'])
                # print(container)
                # print(container['memory_stats']['usage'])
                # print(container['memory_stats']['max_usage'])

                UsageDelta = container['cpu_stats']['cpu_usage']['total_usage'] - container['precpu_stats']['cpu_usage']['total_usage']
                SystemDelta = container['cpu_stats']['system_cpu_usage'] - (container['precpu_stats']['system_cpu_usage'] if 'system_cpu_usage' in container['precpu_stats'] else 0)
                len_cpu = len(container['cpu_stats']['cpu_usage']['percpu_usage'])
                percentage = (UsageDelta / SystemDelta) * len_cpu * 100
                percent = round(percentage, 2)
                print(percent)

                
                # memory_usage = container["memory_stats"]["usage"] - container["memory_stats"]["stats"]["cache"]

                # limit = container["memory_stats"]["limit"]

                # memory_utilization = memory_usage/limit * 100
                # print(memory_utilization)

                mem_used = container["memory_stats"]["usage"] - container["memory_stats"]["stats"]["cache"] + container["memory_stats"]["stats"]["active_file"]
                limit = container['memory_stats']['limit']
                
                mem_percent = round((mem_used / limit * 100), 2)
                print(mem_percent)
                # print(mem_used)
                # print(memory_usage)
                # print(container["memory_stats"]["usage"]/ 10**6)
                # print(UsageDelta, SystemDelta)
                # print(container['cpu_stats']['system_cpu_usage'])
                # print(container['precpu_stats']['system_cpu_usage'] if 'system_cpu_usage' in container['precpu_stats'] else 0)

                # fmt = '%Y-%m-%d %H:%M:%S %Z%z'
                # kurang = dateutil.parser.isoparse(container['read'])-dateutil.parser.isoparse(container['preread'])
                # print(kurang, kurang.total_seconds())
                # print((int(container['cpu_stats']['cpu_usage']['total_usage']) - int(container['precpu_stats']['cpu_usage']['total_usage']))/kurang.total_seconds())

                # print((int(container['cpu_stats']['cpu_usage']['total_usage']) - int(container['precpu_stats']['cpu_usage']['total_usage']))/int(container['precpu_stats']['cpu_usage']['total_usage']), int(container['precpu_stats']['cpu_usage']['total_usage']))

                f = open(filename, "a")
                f.write(f"{container['read']}\t{percent}\t{mem_percent}\n")
                f.close()
            



logging_container('/frosty_cori', "log.flask.txt")
# logging_container('/eager_colden', "log.flask.txt")