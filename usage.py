import docker
import json

def logging_container(name, filename):
    client = docker.from_env()
    print(client.containers.list())
    for i in client.containers.list():
        container = i.stats(stream=False)
        if container['name'] == name:
            for stat in i.stats():
                container = json.loads(stat.decode('UTF-8'))

                percent = round((((container['cpu_stats']['cpu_usage']['total_usage'] - container['precpu_stats']['cpu_usage']['total_usage']) / (container['cpu_stats']['system_cpu_usage'] - (container['precpu_stats']['system_cpu_usage'] if 'system_cpu_usage' in container['precpu_stats'] else 0))) * (len(container['cpu_stats']['cpu_usage']['percpu_usage'])) * 100), 2)
                # print(percent)
                
                mem_percent = round(((container["memory_stats"]["usage"] - container["memory_stats"]["stats"]["cache"] + container["memory_stats"]["stats"]["active_file"]) / (container['memory_stats']['limit']) * 100), 2)
                # print(mem_percent)

                f = open(filename, "a")
                f.write(f"{container['read']}\t{percent}\t{mem_percent}\n")
                f.close()
            
logging_container('/frosty_cori', "log.flask.txt")
# logging_container('/eager_colden', "log.gunicorn.w4.txt")